module.exports = function(app) {
    var mysql = require('mysql');
    var md5 = require('MD5');
    var database = require('../../config/database');
    // var connection;

    // function handleDisconnect() {
    //   connection = mysql.createConnection(database.credentials,function(err) {              // The server is either down
    //     if(err) {                                     // or restarting (takes a while sometimes).
    //       setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    //       console.log('error when connecting to db:', err);
    //     }                                     // to avoid a hot loop, and to allow our node script to
    //   }); // Recreate the connection, since
    //                                           // If you're also serving http, display a 503 error.
    //   connection.on('error', function(err) {
    //     console.log('db error', err);
    //     if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
    //       handleDisconnect();                         // lost due to either server restart, or a
    //     } else {                                      // connnection idle timeout (the wait_timeout
    //       throw err;                                  // server variable configures this)
    //     }
    //   });
    // }

    // handleDisconnect();
    var d = new Date();

    Date.prototype.getWeek = function() {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    }

    // connection.connect();
    var connection  = mysql.createPool({
        connectionLimit : 10,
        host     : 'localhost',
        user     : 'timetable',
        password : 'nfqvntq,k',
        database : 'timetable'
    });

    // var connection = pool;

    app.get('/api', function(req, res) {
        res.send("API is up \& running\nCurrent version is 1.0.0");
    });
    // api ---------------------------------------------------------------------
    // get all lessons



    require('./api/authorization')(app, connection, md5, d);
    require('./api/evaluations')(app, connection, md5, d);
    require('./api/lessons')(app, connection, md5, d);
    require('./api/tracker')(app, connection, md5, d);
    require('./api/events')(app, connection, md5, d);
    require('./api/news')(app, connection, md5, d);
    require('./api/manage')(app, connection, md5, d);

    // application -------------------------------------------------------------


    app.get('/', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });



};