module.exports = function(app, connection, md5, d) {
    var version = undefined;
    var month = d.getMonth();
    month++;
    var current_year = d.getFullYear();
    var current_day = d.getDate() + "/" + month;
    if (month >= 9 && month <= 12) {
        version = '1';
    }
    if (month >= 1 && month <= 3) {
        version = '2';
    }
    if (month >= 3 && month <= 6) {
        version = '3';
    }
    version = current_year + '_' + version;

    var parity = d.getWeek() % 2;
    if (parity) {
        parity = 1;
    }
    if (!parity) {
        parity = 2;
    }

    app.post('/api/lessons/', function(req, res) {
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;

        var sql = 'SELECT * FROM `' + department + '` WHERE `version`="' + version + '" AND `speciality`="' + speciality + '" AND `yoa`="' + year + '" AND (`parity`="' + parity + '" OR `parity`=0) AND `group`="' + group + '" AND (`subgroup`="' + subgroup + '" OR `subgroup`=0) OR `array_of_days` LIKE "%' + current_day + '%"';
        console.log(sql);
        connection.query(sql, function(err, rows, fields) {
          if (!err) {
                lessons = [];
                days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
                for (var dayKey = 0; dayKey < days.length; dayKey++) {
                    var lessonInDay = [];
                    for (var i = rows.length - 1; i >= 0; i--) {
                        if(rows[i].day == days[dayKey]) {
                            lessonInDay.push({
                                array_of_days: rows[i].array_of_days,
                                day: rows[i].day,
                                id: rows[i].id,
                                lesson: rows[i].lesson.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, ""),
                                room: rows[i].room.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, ""),
                                teacher: rows[i].teacher.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, ""),
                                time_from: rows[i].time_from,
                                time_to: rows[i].time_to,
                                parity: rows[i].parity,
                                position: rows[i].position,
                                complexity: rows[i].complexity,
                                type: rows[i].type.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, "")
                            });
                        }
                    };
                    lessonInDay.sort(function(b, a){
                        time_from = {a:a.time_from.split(':'),b:b.time_from.split(':')};
                        return time_from.a[0]-time_from.b[0];
                    });
                    for (var i = lessonInDay.length - 1; i >= 0; i--) {
                        lessons.push(lessonInDay[i]);
                    };
                };
                res.status(200).send(lessons);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "pull_error"
                });
            }
        });
    });

    app.post('/api/lessons/short', function(req, res) {
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;

        var sql = 'SELECT * FROM `' + department + '` WHERE `version`="' + version + '" AND `speciality`="' + speciality + '" AND `yoa`="' + year + '" AND (`parity`="' + parity + '" OR `parity`=0) AND `group`="' + group + '" AND (`subgroup`="' + subgroup + '" OR `subgroup`=0) OR `array_of_days` LIKE "%' + current_day + '%"';
        connection.query(sql, function(err, rows, fields) {
          var lessons = {};
          if (!err) {
                days = ['Monday','Tuesday','Wednesday','Thursday','Friday'];
                for (var dayKey = days.length - 1; dayKey >= 0; dayKey--) {
                    var lessonInDay = {};
                    for (var lessonKey = rows.length - 1; lessonKey >= 0; lessonKey--) {
                        if(rows[lessonKey].day == days[dayKey]) {
                            lessonInDay["less"+lessonKey] = {
                                "array_of_days":rows[lessonKey].array_of_days,
                                "day":rows[lessonKey].day,
                                "id":rows[lessonKey].id,
                                "lesson":rows[lessonKey].lesson,
                                "room":rows[lessonKey].room,
                                "teacher":rows[lessonKey].teacher,
                                "time_from":rows[lessonKey].time_from,
                                "time_to":rows[lessonKey].time_to,
                                "type":rows[lessonKey].type
                            };
                        }
                    };
                    lessons[days[dayKey]] = lessonInDay;
                };
                // console.log(lessons);
                res.status(200).send(lessons);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "pull_error"
                });
            }
        });
    });

    app.post('/api/lessons/hash', function(req, res) {
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;



        var sql = 'SELECT * FROM `' + department + '` WHERE `version`="' + version + '" AND `speciality`="' + speciality + '" AND `yoa`="' + year + '" AND (`parity`="' + parity + '" OR `parity`=0) AND `group`="' + group + '" AND (`subgroup`="' + subgroup + '" OR `subgroup`=0) OR `array_of_days` LIKE "%' + current_day + '%"';
        connection.query(sql, function(err, rows, fields) {
            var hash = "";
            if (!err) {
                for (var i = rows.length - 1; i >= 0; i--) {
                    hash += rows[i].time_from;
                    hash += rows[i].array_of_days;
                    hash += rows[i].complexity;
                    hash += rows[i].day;
                    hash += rows[i].group;
                    hash += rows[i].lesson;
                    hash += rows[i].notification;
                    hash += rows[i].parity;
                    hash += rows[i].position;
                    hash += rows[i].room;
                    hash += rows[i].speciality;
                    hash += rows[i].subgroup;
                    hash += rows[i].teacher;
                    hash += rows[i].time_from;
                    hash += rows[i].time_to;
                    hash += rows[i].type;
                    hash += rows[i].use_days;
                    hash += rows[i].version;
                    hash += rows[i].window;
                    hash += rows[i].yoa;
                };
                hash = md5(hash.toString());
                res.status(200).send(hash);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "pull_error"
                });
            }
        });
    });
};
