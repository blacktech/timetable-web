module.exports = function(app, connection, md5, d) {
    var version = undefined;
    var month = d.getMonth();
    month++;
    var current_year = d.getFullYear();
    var current_day = d.getDate() + "/" + month;
    if (month >= 9 && month <= 12) {
        version = '1';
    }
    if (month >= 1 && month <= 3) {
        version = '2';
    }
    if (month >= 3 && month <= 6) {
        version = '3';
    }
    version = current_year + '_' + version;

    var parity = d.getWeek() % 2;
    if (parity) {
        parity = 1;
    }
    if (!parity) {
        parity = 2;
    }

    app.post('/api/manage/lessons', function(req, res) {
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;
        console.log(req.body);
        var sql = 'SELECT * FROM `' + department + '` WHERE `version`="' + version + '" AND `speciality`="' + speciality + '" AND `yoa`="' + year + '" AND `group`="' + group + '"';
        console.log(sql);
        connection.query(sql, function(err, rows, fields) {
          if (!err) {
                lessons = [];
                days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
                for (var dayKey = 0; dayKey < days.length; dayKey++) {
                    var lessonInDay = [];
                    for (var i = rows.length - 1; i >= 0; i--) {
                        if(rows[i].day == days[dayKey]) {
                            lessonInDay.push({
                                array_of_days: rows[i].array_of_days,
                                day: rows[i].day,
                                id: rows[i].id,
                                lesson: rows[i].lesson.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, ""),
                                room: rows[i].room.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, ""),
                                teacher: rows[i].teacher.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, ""),
                                time_from: rows[i].time_from,
                                time_to: rows[i].time_to,
                                parity: rows[i].parity,
                                position: rows[i].position,
                                subgroup: rows[i].subgroup,
                                complexity: rows[i].complexity,
                                type: rows[i].type.replace(/\t/g, "").replace(/\r/g, "").replace(/\n/g, "")
                            });
                        }
                    };
                    lessonInDay.sort(function(b, a){
                        time_from = {a:a.time_from.split(':'),b:b.time_from.split(':')};
                        return time_from.a[0]-time_from.b[0];
                    });
                    for (var i = lessonInDay.length - 1; i >= 0; i--) {
                        lessons.push(lessonInDay[i]);
                    };
                };
                res.status(200).send(lessons);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "pull_error",
                    "error":err
                });
            }
        });
    });

    app.post('/api/manage/lesson', function(req, res) {
        var lesson = req.body.lesson;
        var userInfo = req.body.userInfo;
        console.log(lesson,userInfo);

        var sql=  'INSERT INTO `'+userInfo.department+'` (`day`, `lesson`, `type`, `room`, `teacher`, `time_from`, `time_to`, `parity`,`speciality`,`yoa`,`group`,`subgroup`) VALUES ("' + lesson.day + '", "' + lesson.lesson + '", "' + lesson.type + '", "' + lesson.room + '", "' + lesson.teacher + '", "' + lesson.time_from + '", "' + lesson.time_to + '", "' + lesson.parity + '","'+userInfo.speciality+'","'+userInfo.year+'","'+userInfo.group+'","'+userInfo.subgroup+'")';
        console.log(sql);
        connection.query(sql,lesson,function(err, rows, fields){
          if (!err) {
                res.status(200).send({
                    "response": true,
                    "reason": "lesson updated"
                });
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error_updating_lesson",
                    "error": err
                });
            }
        });
    });

	app.put('/api/manage/lesson/order', function(req, res) {
        var lessons = req.body.lessons;
        // var userInfo = req.body.userInfo;
        // console.log(lesson,userInfo);

        // var sql=  'UPDATE `'+userInfo.department+'` SET `day` = "'+lesson.day+'", `room` = "'+lesson.room+'", `lesson` = "'+lesson.lesson+'", `parity` = '+lesson.parity+', `type` = "'+lesson.type+'", `teacher` = "'+lesson.teacher+'", `time_from` = "'+lesson.time_from+'", `time_to` = "'+lesson.time_to+'" WHERE `id`= '+lesson.id;
        // console.log(sql);
        // connection.query(sql, function(err, rows, fields){
        //   if (!err) {
        //         res.status(200).send({
        //             "response": true,
        //             "reason": "lesson updated"
        //         });
        //     } else {
        //         res.status(500).send({
        //             "response": false,
        //             "reason": "error_updating_lesson",
        //             "error": err
        //         });
        //     }
        // });
    });

    app.put('/api/manage/lesson', function(req, res) {
        var lesson = req.body.lesson;
        var userInfo = req.body.userInfo;
        console.log(lesson,userInfo);

        var sql=  'UPDATE `'+userInfo.department+'` SET `day` = "'+lesson.day+'", `room` = "'+lesson.room+'", `lesson` = "'+lesson.lesson+'", `parity` = '+lesson.parity+', `type` = "'+lesson.type+'", `teacher` = "'+lesson.teacher+'", `time_from` = "'+lesson.time_from+'", `time_to` = "'+lesson.time_to+'" WHERE `id`= '+lesson.id;
        console.log(sql);
        connection.query(sql, function(err, rows, fields){
          if (!err) {
                res.status(200).send({
                    "response": true,
                    "reason": "lesson updated"
                });
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error_updating_lesson",
                    "error": err
                });
            }
        });
    });

    app.delete('/api/manage/lesson/:id', function(req, res) {
        var lessonId = req.params.id;
        var userInfo = req.body.userInfo;

        console.log(lessonId,userInfo);
        var sql=  'DELETE FROM `'+userInfo.department+'` WHERE `id`='+lessonId;
        console.log(sql);
		connection.query(sql, function(err, result){
		  if (!err && result.affectedRows==1) {
                res.status(200).send({
                    "response": true,
                    "reason": "lesson deleted"
                });
            } else {
                var reason = "error_updating_lesson";
                if (result.affectedRows>1) {
                    reason = "more_than_one_row";
                }
                if (result.affectedRows==0) {
                    reason = "lesson_not_found";
                }
                res.status(500).send({
                    "response": false,
                    "reason": reason,
                    "error": err
                });
            }
		});
    });
}
