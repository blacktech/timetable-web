module.exports = function(app, connection, md5, d) {
    app.get('/api/news', function(req, res) {
        var sql = 'SELECT * FROM news ORDER BY id';
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                for (var i = rows.length - 1; i >= 0; i--) {
                    rows[i].id = rows[i].id.toString();
                };
                res.status(200).send(rows);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "pull_error",
                    "error": err
                });
            }
        });
    });

    app.get('/api/news/:id', function(req, res) {
        var id = req.params.id;
        var sql = 'SELECT * FROM news WHERE id=' + id + ' LIMIT 1';
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                for (var i = rows.length - 1; i >= 0; i--) {
                    rows[i].id = rows[i].id.toString();
                };
                res.status(200).send(rows);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "pull_error",
                    "error": err
                });
            }
        });
    });
}
