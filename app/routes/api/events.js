module.exports = function(app, connection, md5, d) {
    app.post('/api/events', function(req, res) {
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;

        var sql = 'SELECT * FROM `events` WHERE `department`="' + department + '" AND `speciality`="' + speciality + '" AND `yoa`="' + year + '" AND `group`="' + group + '"';
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                var events = [];
                for (var i = rows.length - 1; i >= 0; i--) {
                    events.push({
                        id:rows[i].id.toString(),
                        content:rows[i].content,
                        date:rows[i].date,
                        time:rows[i].time
                    });
                };
                res.status(200).send(events);
            } else {
                // throw err;
                res.status(500).send("Error pulling events");
            }
        });
    });

    app.post('/api/events/add', function(req, res) {
        var content = req.body.content;
        var time = req.body.time;
        var date = req.body.date;
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;
        // var hash = req.body.hash;
        console.log(req.body);
        // var user;
        // connection.query('SELECT * FROM users WHERE `hash`="' + hash + '" LIMIT 1', function(err, user, fields) {
        //     user = user[0];
        // });
        var sql = 'INSERT INTO events (`content`, `date`, `time`, `department`, `yoa`, `group`, `subgroup`, `speciality`) VALUES ("' + content + '","' + date + '","' + time + '", "' + department + '", "' + year + '", "' + group + '", "' + subgroup + '", "' + speciality + '")';
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                // if (user.access=="groupmaster") {
                    res.status(201).send({
                        "response": true,
                        "reason": "event created"
                    });
                // } else {
                //     res.status(403).send({
                //         "response": false,
                //         "reason": "Access denied"
                //     });
                // }
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error_add_event",
                    "error": err
                });
            }
        });
    });

    app.put('/api/events/:id', function(req, res) {
        var id = req.params.id;
        var time = req.body.time;
        var date = req.body.date;
        var content = req.body.content;

        var sql = 'UPDATE `events` SET `content`="' + content + '", `date`="' + date + '", `time`="' + time + '"  WHERE id="' + id + '"';
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                res.status(200).send({
                    "response": true,
                    "reason": "event updated"
                });
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error_updating_event",
                    "error": err
                });
            }
        });
    });

    app.delete('/api/events/:id', function(req, res) {
        var id = req.params.id;

        var sql = 'DELETE FROM `events` WHERE id="' + id + '"';
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                res.status(200).send({
                    "response": true,
                    "reason": "event deleted"
                });
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error_deleting_event",
                    "error": err
                });
            }
        });
    });
};
