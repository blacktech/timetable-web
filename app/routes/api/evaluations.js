module.exports = function(app, connection, md5, d) {
    app.post('/api/evaluations/', function(req, res) {
        var id = req.body.id;
        var hash = req.body.hash;

        var sql = 'SELECT * FROM users WHERE `id`=' + id + ' AND `hash`="' + hash + '" LIMIT 1';
        connection.query(sql, function(err, rows, fields) {
            user = rows[0];
            if (!err) {
/*                if (user['evaluations'].id == undefined) {
                    s = user['evaluations'].toString();
                } else {
                    s = user['evaluations'].toString();
                    s = s.substr(1, s.length - 2);
                }*/
                // console.log(JSON.parse(user['evaluations'].toString()).length);
                if (user == undefined) {
                    res.redirect('/#/logout');
                };
                res.status(200).send(user['evaluations']);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error_getting_evaluations",
                    "error": err
                });
            }
        });
    });

    app.put('/api/evaluations/', function(req, res) {
        var id = req.body.id;
        var hash = req.body.hash;
        var evaluations = req.body.evaluations;

        var sql = "UPDATE `users` SET `evaluations`='" + evaluations + "' WHERE id='" + id + "'";
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                res.status(200).send({
                    "response": true,
                    "reason": "evaluations updated"
                });
            } else {
                // throw(err);
                res.status(500).send({
                    "response": false,
                    "reason": "error updating evaluations",
                    error: err
                });
            }
        });
    });

    app.put('/api/evaluations/complexity', function(req, res) {
        var id = req.body.id;
        var hash = req.body.hash;
        var prev_mark = req.body.prev_mark;
        var new_mark = req.body.new_mark;
        var department = req.body.department;
        var new_complexity = 0;

        var sql = "SELECT * FROM `" + department + "` WHERE `id`=" + id + " LIMIT 1";
        connection.query(sql, function(err, rows, fields) {
            if (!err) {
                lesson = rows[0];
                complexity = lesson['complexity'];

                if (prev_mark != 0) {
                    new_complexity -= prev_mark;
                    new_complexity += new_mark;
                }
                if (new_complexity == '' || prev_mark == 0) {
                    new_complexity += new_mark;
                }
                var sql = "UPDATE `" + department + "` SET `complexity`='" + new_complexity + "' WHERE id='" + id + "'";
                connection.query(sql, function(err, rows, fields) {
                    if (!err) {
                        res.status(200).send({
                            "response": true,
                            "reason": "complexity updated"
                        });
                    } else {
                        // throw(err);
                        res.status(500).send({
                            "response": false,
                            "reason": "error updating complexity",
                            error: err
                        });
                    }
                });
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "error receiving complexity",
                    error: err
                });
            }
        });
    });
};
