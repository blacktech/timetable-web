module.exports = function(app, connection, md5, d) {
    // authorizing user
    app.post('/api/auth/login', function(req, res) {
        console.log(req.body);
        if (req.body.login!=undefined && req.body.password!=undefined) {
            var login = req.body.login;
            var password = req.body.password;
            connection.query('SELECT * FROM users WHERE `login`="' + login + '" AND `password`="' + password + '" LIMIT 1', function(err, user, fields) {
                user = user[0];
                if (user != undefined && user != null && user != "") {
                    var hash = md5(login + password + d.toString());
                    connection.query('UPDATE users SET `hash`="' + hash + '" WHERE id=' + user.id + '', function(err) {
                        if (!err) {
                            res.status(200).send({
                                "response": true,
                                "reason": "all success",
                                "uid": "" + user['id'] + "",
                                "hash": "" + hash + "",
                                "user_info": {
                                    "department": "" + user['department'] + "",
                                    "group": "" + user['group'] + "",
                                    "speciality": "" + user['speciality'] + "",
                                    "subgroup": "" + user['subgroup'] + "",
                                    "year": "" + user['yoa'] + "",
                                    "access": "" + user['access'] + ""
                                }
                            });
                        } else {
                            res.status(500).send({
                                "response": false,
                                "reason": "hash_create",
                                "error": "+err+"
                            });
                        }
                    });
                } else {
                    // if (err) throw err;
                    res.status(404).send({
                        "response": false,
                        "reason": "not_exist"
                    })

                }
            });
        } else {
            res.status(500).send({
                "response": false,
                "reason": "wrong object",
                "object": req.body
            });
        }
    });

    // checking authorization user
    app.post('/api/auth/check', function(req, res) {
        var id = req.body.id;
        var hash = req.body.hash;
        connection.query('SELECT * FROM users WHERE `id`="' + id + '" AND `hash`="' + hash + '" LIMIT 1', function(err, user, fields) {
            user = user[0];
            if (user != undefined) {
                // if (err) throw err;
                if (id == '' || hash == '' || user['id'] != id || user['hash'] != hash) {
                    res.status(401).send({
                        "response": false,
                        "reason": "user_not_found"
                    });
                } else {
                    department = user['department'];
                    group = user['group'];
                    speciality = user['speciality'];
                    subgroup = user['subgroup'];
                    year = user['yoa'];
                    access = user['access'];
                    res.status(200).send({
                        "response": true,
                        "reason": "all success",
                        "uid": "" + user['id'] + "",
                        "hash": "" + hash + "",
                        "user_info": {
                            "department": "" + user['department'] + "",
                            "group": "" + user['group'] + "",
                            "speciality": "" + user['speciality'] + "",
                            "subgroup": "" + user['subgroup'] + "",
                            "year": "" + user['yoa'] + "",
                            "access": "" + user['access'] + ""
                        }
                    });
                }
            } else {
                res.status(401).send({
                    "response": false,
                    "reason": "user_not_found",
                    "error" : err
                });
            }
        });
    });

    app.post('/api/auth/reg', function(req, res) {
        console.log(req.body);
        var login = req.body.login;
        var password = req.body.password;
        var email = req.body.email;
        var year = req.body.year;
        var department = req.body.department;
        var speciality = req.body.speciality;
        var group = req.body.group;
        var subgroup = req.body.subgroup;
        var hash = md5(login + password + d);

        connection.query('SELECT * FROM users WHERE `login`="' + login + '" AND `email`="' + email + '" LIMIT 1', function(err, user, fields) {
            user = user[0];
            if (user != undefined && user != "") {
                res.status(500).send({
                    "response": false,
                    "reason": "already_exist"
                });
            } else {
                console.log('INSERT INTO users (`login`, `password`, `email`, `department`, `yoa`, `group`, `subgroup`, `speciality`,`hash`,`evaluations`,`track`) VALUES ("' + login + '", "' + password + '", "' + email + '", "' + department + '", "' + year + '", "' + group + '", "' + subgroup + '", "' + speciality + '","' + hash + '","[]","[]")');
                connection.query('INSERT INTO users (`login`, `password`, `email`, `department`, `yoa`, `group`, `subgroup`, `speciality`,`hash`,`evaluations`,`track`) VALUES ("' + login + '", "' + password + '", "' + email + '", "' + department + '", "' + year + '", "' + group + '", "' + subgroup + '", "' + speciality + '","' + hash + '","[]","[]")', function(err, user, fields) {
                    if (!err) {
                        connection.query('SELECT * FROM users WHERE `password`="' + password + '" AND `login`="' + login + '" LIMIT 1', function(err, new_user, fields) {
                            new_user = new_user[0];
                            res.status(201).send({
                                "response": true,
                                "reason": "all success",
                                "uid": new_user['id'],
                                "hash": hash
                            });
                        });
                    } else {
                        res.status(500).send({
                            "response": false,
                            "reason": "create_error",
                            "error": err
                        });
                    }
                });
            }
        });
    });
};
