module.exports = function(app, connection, md5, d) {
    app.get('/api/users/:login', function(req, res) {
        var login = req.params.login;
        var hash = req.body.hash;

        connection.query("SELECT * FROM `users` WHERE `login` LIKE '%" + login + "%'", function(err, users, fields) {
            if (!err) {
                foundUsers = [];
                for (var i = users.length - 1; i >= 0; i--) {
                    foundUsers.push({
                        login: users[i].login,
                        id: users[i].id,
                        year: users[i].yoa,
                        department: users[i].department,
                        speciality: users[i].speciality,
                        group: users[i].group,
                        subgroup: users[i].subgroup
                    });
                };
                res.status(200).send(foundUsers);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "user_searching_user"
                });
            }
        });
    });

    app.post('/api/tracker/track', function(req, res) {
        var userInitiatorId = req.body.id;
        var userInitiatorHash = req.body.hash;
        var userTrackLogin = req.body.userTrackLogin;
        var userTrackId = req.body.userTrackId;

        var userTrackIsEmpty = false;
        var userInitiatorIsEmpty = false;

        var errors = {
            userTrackQueryError: false,
            userInitiatorQueryError: false
        };

        var data = {
            userTrackData: undefined,
            userInitiatorData: undefined
        }

        connection.query("SELECT * FROM `users` WHERE `login` = '" + userTrackLogin + "' AND `id`=" + userTrackId + " LIMIT 1", function(userTrackQueryError, user, fields) {
            data.userTrackData = user[0];
            errors.userTrackQueryError = userTrackQueryError;

            connection.query("SELECT * FROM `users` WHERE `id`=" + userInitiatorId + " AND `hash`='" + userInitiatorHash + "' LIMIT 1", function(userInitiatorQueryError, user, fields) {
                data.userInitiatorData = user[0];
                errors.userInitiatorQueryError = userInitiatorQueryError;

                if (errors.userTrackQueryError || errors.userInitiatorQueryError) {
                    if (errors.userTrackQueryError) {
                        res.status(500).send({
                            result: false,
                            type: "userTrackQuery",
                            error: userTrackQueryError
                        }).end();
                    };
                    if (errors.userInitiatorQueryError) {
                        res.status(500).send({
                            result: false,
                            type: "userInitiatorQuery",
                            error: userInitiatorQueryError
                        }).end();
                    };
                } else {
                    if (data.userTrackData == undefined || data.userInitiatorData == undefined) {
                        if (data.userTrackData == undefined) userTrackIsEmpty = true;
                        if (data.userInitiatorData == undefined) userInitiatorIsEmpty = true;
                        res.status(500).send({
                            result: false,
                            userTrackIsEmpty: userTrackIsEmpty,
                            userInitiatorIsEmpty: userInitiatorIsEmpty
                        }).end();
                    } else {
                        trackData = JSON.parse(data.userInitiatorData['track']);
                        try {
                            if (trackData[0].length === 0) {
                                trackData = [{
                                    id: userTrackId,
                                    login: userTrackLogin
                                }];
                            }
                        } catch (err) {
                            trackData.push({
                                id: userTrackId,
                                login: userTrackLogin
                            });
                        }

                        trackData = JSON.stringify(trackData);

                        connection.query("UPDATE `users` SET `track`='" + trackData + "' WHERE `id`=" + userInitiatorId + "", function(updateTrackDataError, user, fields) {
                            if (!updateTrackDataError) {
                                res.status(201).send({
                                    result: true,
                                    type: "all success"
                                }).end();
                            } else {
                                res.status(500).send({
                                    result: false,
                                    type: "userTrackError",
                                    error: updateTrackDataError
                                }).end();
                            }
                        });
                    }
                }
            });
        });
    });

    app.post('/api/tracker/data', function(req, res) {
        var id = req.body.id;
        var hash = req.body.hash;

        connection.query("SELECT * FROM `users` WHERE `id`=" + id + " AND `hash`='" + hash + "' LIMIT 1", function(err, users, fields) {
            user = users[0];
            if (!err && user != undefined) {
                res.status(200).send(user['track']);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "user_getting_tracker_data",
                    error: err || "invalid_user_data"
                });
            }
        });
    });

    app.post('/api/tracker/userData', function(req, res) {
        var login = req.body.login;
        var hash = req.body.hash;

        connection.query("SELECT * FROM `users` SELECT `login`='" + login + "' AND `hash`='" + hash + "' LIMIT 1", function(err, users, fields) {
            if (!err) {
                foundUsers = [];
                for (var i = users.length - 1; i >= 0; i--) {
                    foundUsers.push({
                        login: users[i].login,
                        id: users[i].id,
                        year: users[i].yoa,
                        department: users[i].department,
                        speciality: users[i].speciality,
                        group: users[i].group,
                        subgroup: users[i].subgroup
                    });
                };
                res.status(200).send(foundUsers);
            } else {
                res.status(500).send({
                    "response": false,
                    "reason": "user_getting_tracker_data",
                    error: err
                });
            }
        });
    });
};
