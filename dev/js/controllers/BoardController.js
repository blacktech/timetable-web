var evaluationChart;
Board = angular.module('Board', []);

Board.controller('boardController',function($scope, $http, $sce, $modal, $sessionStorage, $timeout, API_HOST, dateFilter, Auth, $location, appCookie) {
    $scope.next_week = false;
    $scope.is_lesson = undefined;
    $scope.evaluations = undefined;
    $scope.activity = undefined;

    if ($sessionStorage.data!=undefined) {
        $scope.access = $sessionStorage.data.access;
    };

    $scope.complexity_stages = [
        {value:1,name:"1"},
        {value:2,name:"2"},
        {value:3,name:"3"},
        {value:4,name:"4"},
        {value:5,name:"5"}
    ];

    Auth.check();
    pull_lessons();

    d = new Date();
    $scope.loaded = false;
    $scope.days = [
        {
            day:'Monday',
        },{
            day:'Tuesday',
        },{
            day:'Wednesday',
        },{
            day:'Thursday',
        },{
            day:'Friday',
        },{
            day:'Sunday',
        },{
            day:'Saturday',
        }
    ];

    $scope.data = $sessionStorage.data;

    function pull_lessons() {
        $http({method: 'POST',url: API_HOST+'/lessons', data: $sessionStorage.data, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            $scope.lessons = data;
            console.log(data,'lessons');
            angular.forEach($scope.lessons,function (value, key) {
                value.now = undefined;
            });
            user_data = {
                "id":appCookie.getCookie('id'),
                "hash":appCookie.getCookie('hash')
            };
            $http({method: 'POST',url: API_HOST+'/evaluations', data: user_data, headers: {'Content-Type': 'application/json'},
            }).success(function(data) {
                console.log(data,'evals');
                $scope.evaluations = data;
                $scope.evaluations = parseFromArrayToJson($scope.evaluations);
                console.log($scope.evaluations, 'evals parse result');
                init_chart();

            });
        }).
        error(function(data) {
            console.log(data);
        });
    }

    $scope.isExistEval = function(id) {
        var res = false;
        if (typeof $scope.evaluations=='object') {
            if ($scope.evaluationSearch('lesson_id',id)!=undefined) {
                res = true;
            };
        };
        return res;
    }

    $scope.set_hardity = function(hardity) {
        if($scope.isExistEval($scope.modal_lesson.id)) {
            $scope.evaluations[$scope.evaluationSearch('lesson_id',$scope.modal_lesson.id)].value = hardity.value;
            update_lesson_complexity($scope.modal_lesson.id,$scope.evaluations[$scope.evaluationSearch('lesson_id',$scope.modal_lesson.id)].value,hardity.value);
        } else {
            if($scope.evaluations!={} && $scope.evaluations!=undefined) {
                $scope.evaluations.push({"lesson_id":$scope.modal_lesson.id,"rated":true,"value":hardity.value});
            } else {
                $scope.evaluations = [{"lesson_id":$scope.modal_lesson.id,"rated":true,"value":hardity.value}];
            }
            update_lesson_complexity($scope.modal_lesson.id,0,hardity.value);
        }
        $scope.modalLessonValue = hardity.value;
        update_evaluations();
        $scope.evaluations = parseFromArrayToJson($scope.evaluations);
        pull_lessons();
        // console.log($scope.evaluations);
    };

    function update_lesson_complexity(lid,prev_mark,new_mark) {
        var obj = {"id":lid,"prev_mark":prev_mark,"new_mark":new_mark,"department":$sessionStorage.data.department};
        $http({method: 'PUT',url: API_HOST+'/evaluations/complexity', data: obj, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            // console.log(data);
        }).
        error(function(data) {
            console.log(data);
        });
    }

    $scope.getHardity = function(id) {
         if (typeof $scope.evaluations=='object') {
            if ($scope.evaluationSearch('lesson_id',id)!=undefined) {
                 for (var i=0 ; i < $scope.evaluations.length ; i++)
                    {
                        if ($scope.evaluations[i]["lesson_id"] == id) {
                          var fObj = $scope.evaluations[i].value;
                        }
                    }
                    return fObj;
            };
        };
    };

    $scope.checkEmptyArray = function(array,day) {
        // if (array.length > 0) {
        //     return false;
        // } else {
        //     return true;
        // }
    }

    $scope.modal=function(id) {
        $scope.modalLessonValue = undefined;
        // console.log(index);
        // console.log($scope.lessons[lessonSearch("id",id)]);
        $scope.modal_lesson = $scope.lessons[lessonSearch("id",id)];
        if($scope.evaluations!=undefined) {
            if ($scope.evaluations[$scope.evaluationSearch('lesson_id',$scope.modal_lesson.id)]!=undefined) {
                $scope.modalLessonValue = $scope.evaluations[$scope.evaluationSearch('lesson_id',$scope.modal_lesson.id)].value;
            };
        }
        console.log($scope.modalLessonValue);
    };

    $scope.format = 'HH:mm:ss';
    $scope.time = dateFilter(new Date(), 'HH:mm:ss');

    $scope.currentTime = function(lesson) {
        if($scope.time >= lesson.time_from && $scope.time <= lesson.time_to && lesson.day==$scope.currentDay && (lesson.subgroup == $sessionStorage.data.subgroup || lesson.subgroup == 0)){
            return true;
        }
        return false;
    };

    function lessonSearch(searchField,searchVal) {
            for (var i=0 ; i < $scope.lessons.length ; i++)
            {
                if ($scope.lessons[i][searchField] == searchVal) {
                  var fObj = i;
                }
            }
            return fObj;
    };

    $scope.evaluationSearch = function (searchField,searchVal) {
        if (typeof $scope.evaluations=='object') {
            for (var i=0 ; i < $scope.evaluations.length ; i++)
            {
                if ($scope.evaluations[i][searchField] == searchVal) {
                  var fObj = i;
                }
            }
            return fObj;
        }
    };

    update_evaluations = function() {
        stringed_evaluations = JSON.stringify($scope.evaluations);
        var obj = {"id":appCookie.getCookie('id'),"evaluations":stringed_evaluations};
        $http({method: 'PUT',url: API_HOST+'/evaluations', data: obj, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            $scope.evaluations = parseFromArrayToJson($scope.evaluations);
            console.log(data);
        }).
        error(function(data) {
            console.log(data);
        });
    };

    console.log((dateFilter(new Date(), "dd/MM")));

    $scope.isLessonToday = function(days) {
        var i = false;
        if (days!="" && days!=undefined) {
            days = days.split(",");
            today = dateFilter(new Date(), "dd/MM");
            angular.forEach(days, function(day,key){
                day = day.trim();
                if (today==day) {
                    i=true;
                };
            });
        }
        return i;
    }

    function parseFromArrayToJson(input) {
        if (input!='' && input!=undefined) {
            var newJsonObj = [];
            input = JSON.stringify(input);
            input = input.allReplace({"[\[]":""});
            input = input.allReplace({"[\]]":""});
            input = input.allReplace({"[\}\]]":"}"});
            input = input.split("},{");
            angular.forEach(input, function(value , key) {
                input[key] = input[key].allReplace({"[\{]":""});
                input[key] = input[key].allReplace({"[\}]":""});
            });
            angular.forEach(input, function(value , key) {
                newJsonObj.push((JSON.parse("{"+value+"}")));
            });
            return newJsonObj;
        };
    };

    $scope.clog = function(a) {
        console.log(a);
    };

    function init_chart() {
        function getGroupMark(day) {
            var hardity = 0;
            for (var i=0 ; i < $scope.lessons.length ; i++)
                {
                    if ($scope.lessons[i]['day'] == day) {
                      var hardity = parseInt(hardity)+parseInt($scope.lessons[i].complexity);
                      console.log($scope.lessons[i],$scope.lessons[i].complexity);
                    }
                }
            // console.log("hardity",hardity);
            return parseInt(hardity) || 0;
        }
        function getUserMark(day) {
            var hardity = 0;
            for (var i=0 ; i < $scope.lessons.length ; i++)
                {
                    if (typeof $scope.evaluations=='object') {
                        if ($scope.lessons[i]['day'] == day) {
                            if ($scope.evaluations[$scope.evaluationSearch('lesson_id',$scope.lessons[i].id)]!=undefined) {
                                user_eval = $scope.evaluations[$scope.evaluationSearch('lesson_id',$scope.lessons[i].id)].value;
                                hardity = parseInt(hardity)+parseInt(user_eval);
                            };
                        }
                    }
                }
                return parseInt(hardity) || 0;
        }
        // Our labels and three data series
        //
        var ctx = $("#evaluationChart").get(0).getContext("2d");
        var data = {
            labels: ['Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця'],
            datasets: [
                {
                    label: "Группа",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [getGroupMark('Monday'),getGroupMark('Tuesday'),getGroupMark('Wednesday'),getGroupMark('Thursday'),getGroupMark('Friday')]
                },
                {
                    label: "Ви",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [getUserMark('Monday'),getUserMark('Tuesday'),getUserMark('Wednesday'),getUserMark('Thursday'),getUserMark('Friday')]
                }
            ]
        };
        // This will get the first returned node in the jQuery collection.
        Chart.defaults.global.responsive = true;
        Chart.defaults.global.maintainAspectRatio = false;
        evaluationChart = new Chart(ctx).Line(data);

    }
});

Board.directive('myCurrentTime', function($interval, dateFilter) {
            // return the directive link function. (compile function not needed)
            return function(scope, element, attrs) {
                var format,  // date format
                    stopTime; // so that we can cancel the time updates

                // used to update the UI
                function updateTime() {
                    nl=undefined;
                    element.val(dateFilter(new Date(), format));
                    scope.time = element.val();
                    gtv = scope.time;
                    scope.time_splitted = scope.time.split(":");
                    var l = false;
                    if(scope.lessons!=undefined) {
                        for (var i=0 ; i < scope.lessons.length ; i++)
                            {
                                if(scope.time >= scope.lessons[i].time_from && scope.time <= scope.lessons[i].time_to && scope.lessons[i].day==scope.days[d.getDay()-1].day){
                                    l=true;
                                    scope.nl = i;
                                    scope.lessons[i].now = true;
                                } else {
                                    scope.lessons[i].now = false;
                                }
                            }
                            if(l) {
                                scope.is_lesson = true;
                            }



                        var ll  = true;
                        for (var i=0 ; i < scope.lessons.length ; i++)
                            {
                                var time_from = scope.lessons[i].time_from.split(":");
                                time_from[2] = "00";
                                time_from = time_from[0]+time_from[1]+time_from[2];
                                // console.log(scope.time);
                                if(scope.time >= time_from && scope.time <= scope.lessons[i].time_to && scope.lessons[i].day==scope.days[d.getDay()-1]){
                                    scope.ll=true;
                                    scope.ml = i;
                                }
                            }
                    }

                    var lessons_today = [];
                    var a=-2;
                    var n = d.getHours();

                    var day = scope.currentDay;
                    var time = scope.time;
                    var inside = undefined;

                    if (scope.lessons!=undefined) {
                        for (var i=0 ; i < scope.lessons.length ; i++)
                            {
                                if (scope.lessons[i]["day"] == day) {
                                  lessons_today.push(scope.lessons[i]);
                                  a++;
                                }
                            }
                        };

                    if (lessons_today[0]!=undefined) {
                        var lessonBegin = lessons_today[0].time_from;
                        // console.log(lessons_today[0]);
                        var lessonOver = lessons_today[0].time_to;
                        // console.log(lessons_today[a]);
                        var cli = scope.nl; //current lesson index

                        if (n.length==1) {n= "0"+n};
                        if (lessonBegin<time && time<lessonOver ) {
                            // console.log('actually we\'re should be at the university');
                            inside = true;
                        } else {
                            // console.log('actually we\'re shouldn\'t be at the university');
                            inside = false;
                        }

                        if(inside && day!='Saturday' && day!='Sunday' && scope.is_lesson)
                            {
                                scope.activity = 'lesson';
                                scope.loaded = true;
                            }
                        if(inside && day!='Saturday' && day!='Sunday' && !scope.is_lesson)
                            {
                                scope.activity = 'break';
                                scope.loaded = true;
                             }
                        if(!inside && day!='Saturday' && day!='Sunday' && time>lessonBegin )
                         {
                                scope.activity = 'over';
                                scope.loaded = true;
                            }
                        if(!inside && day!='Saturday' && day!='Sunday' && n>='00' && time<lessonOver)
                            {
                                scope.activity = 'not_started';
                                scope.loaded = true;
                         }
                    } else {
                        if(day=='Saturday' || day=='Sunday')
                            {
                                scope.activity = 'holidays';
                                scope.loaded = true;
                            } else {
                                // scope.loaded = true;
                                // console.log('unkown');
                            }
                    }

                    var dn = d.getDay();
                    if (dn==0) {
                        dn=6;
                    } else {
                        dn=dn-1;
                    }
                    scope.currentDay = scope.days[dn].day;
                    // console.log(scope.activity);
                    // console.log('\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\');
                }

                // watch the expression, and update the UI on change.
                scope.$watch(attrs.myCurrentTime, function(value) {
                    format = value;
                    updateTime();
                });

                stopTime = $interval(updateTime, 1000);

                // listen on DOM destroy (removal) event, and cancel the next UI update
                // to prevent updating time ofter the DOM element was removed.
                element.bind('$destroy', function() {
                    $interval.cancel(stopTime);
                });
            }
        });

countObjects = function (obj) {
    var count = 0;
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }
    return count;
}