Track = angular.module('Track', ['mgcrea.ngStrap']);

Track.controller('trackController',function($scope,$http,$sce,$modal,$sessionStorage, API_HOST, appCookie , Auth) {

	Auth.check();

	$scope.showInfo = false;
	$scope.loaded = false;
	$scope.compareSuccess = false;

	$scope.searchUsers = function(viewValue) {
		var data = {login: viewValue,uid:appCookie.getCookie('id')};

		return $http({method: 'GET',url: API_HOST+'/users/'+viewValue, headers: {'Content-Type': 'application/json'},
			}).then(function(res) {
				console.log(res.data,'found data');
				$scope.foundUsers = res.data;
				angular.forEach($scope.foundUsers,function(userFromSearch, searchKey){
					angular.forEach($scope.tracker,function(userFromTrack, trackerKey){
						if(userFromTrack.login == userFromSearch.login)
							{
								$scope.foundUsers.splice(searchKey,1);
							}
					});
				});
				return $scope.foundUsers;
		});
	};

	$scope.addUser = function() {
		data = {
			id:appCookie.getCookie('id'),
			hash:appCookie.getCookie('hash'),
			userTrackId:$scope.foundUsers[0].id,
			userTrackLogin:$scope.foundUsers[0].login
		};

		console.log(data,'addUserData');

		$http({method: 'POST',url: API_HOST+'/tracker/track', data: data, headers: {'Content-Type': 'application/json'},
			}).success(function(data) {
				$scope.userToTrack = "";
				console.log(data)
				$scope.pull();
		});
	};

	$scope.compareNicknames = function() {
		var flag = false;
		angular.forEach($scope.foundUsers, function(value,key) {
			if(value.login==$scope.userToTrack) {
				flag = true;
			}
		});

		if (flag) {
			$scope.compareSuccess = true;
		} else {
			$scope.compareSuccess = false;
		}
	};

	$scope.pull = function() {
		var initiatorData = {
			id:appCookie.getCookie('id'),
			hash:appCookie.getCookie('hash'),
		};

		$http({method: 'POST',url: API_HOST+'/tracker/data', data: initiatorData, headers: {'Content-Type': 'application/json'},
			}).success(function(data) {
				var usersToTrack = data;
				angular.forEach(usersToTrack, function(value, key) {
					var userInfoData = {
						id:value.id,
						login:value.login
					}
					$http({method: 'GET',url: API_HOST+'/users/'+userInfoData.login, headers: {'Content-Type': 'application/json'},
					}).success(function(data) {
						value.user_data = data[0];
						$http({method: 'POST',url: API_HOST+'/lessons', data: value.user_data, headers: {'Content-Type': 'application/json'},
						}).success(function(data) {
							console.log(value.user_data,'user data')
							console.log(data,'lesson data')
							if (!data.result) {
								switch(data.reason) {
									case "holidays":
										$scope.activity = "holidays";
									break;
								}
							} else {
								usersToTrack[key].lesson_data = data;
							}
						});
					});

				});
				$scope.tracker = usersToTrack;
				console.log($scope.tracker,'tracker init');
				$scope.loaded = true;
		});
	};

	$scope.pull();
});

Track.config(function($typeaheadProvider) {
  angular.extend($typeaheadProvider.defaults, {
    minLength: 2,
    limit: 8
  });
})

countObjects = function (obj) {
	var count = 0;
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }
    return count;
}