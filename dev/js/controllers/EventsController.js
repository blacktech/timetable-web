Events = angular.module('Events', ['xeditable']);

Events.controller('eventsController',function($scope,$http,$sce,$modal,$sessionStorage, API_HOST) {
  	$scope.access = $sessionStorage.data.access;
  	$scope.loaded = false;

  	$scope.pull = function() {
		$http({method: 'POST',url: API_HOST+'/events', data: $sessionStorage.data, headers: {'Content-Type': 'application/json'},
	    	}).success(function(data) {
	      		$scope.events = data;
	            console.log(data,'events');
	            console.log(data[data.length-1],'info');
	  			$scope.loaded = true;
	    	}).
	    	error(function(data) {
	            console.log(data);
	    	});
  	}
  	$scope.pull();

    $scope.updateEvent = function(index) {
        console.log($scope.events[index]);
    	$http({method: 'PUT',url: API_HOST+'/events/'+$scope.events[index].id, data: $scope.events[index], headers: {'Content-Type': 'application/json'},
    	}).success(function(data) {
            console.log(data,'update event #'+index+':success');
    	}).
    	error(function(data) {
            console.log(data);
    	});
    };


    $scope.addEvent = function() {
        var data = $sessionStorage.data;
        data.time = $scope.event.time;
        data.date = $scope.event.date;
        data.content = $scope.event.content;
        $http({method: 'POST',url: API_HOST+'/events/add', data: data, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            console.log(data,'addEvent:success');
        }).
        error(function(data) {
            console.log(data,'addEvent:error');
        });
    };

    $scope.deleteEvent = function(index) {
    	var data = {
    		id:$scope.events[index].id,
    	}
    	$http({method: 'DELETE',url: API_HOST+'/events/'+data.id, data: data, headers: {'Content-Type': 'application/json'},
    	}).success(function(data) {
            console.log(data,'delete event #'+index+':success');
            $scope.pull();
    	}).
    	error(function(data) {
            console.log(data);
    	});
    };
});

Events.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});

countObjects = function (obj) {		
	var count = 0;
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }
    return count;
}