News = angular.module('News', []);

News.controller('newsController',function($scope,$http,$sce,$modal,$sessionStorage, API_HOST) {
  	
  	$scope.loaded = false;

	$http({method: 'GET',url: API_HOST+'/news', data: '', headers: {'Content-Type': 'application/json'},
	}).success(function(data) {
  		$scope.articles = data;
		angular.forEach($scope.articles, function(news , key) {
			news.content = news.content.replace(/\[F\]/g,'<i class="fa fa-cog"></i>');
			news.content = news.content.replace(/\[\+\]/g,'<i class="fa fa-plus"></i>');
			news.content = $sce.trustAsHtml(news.content);
		})
		console.log(data);
  		$scope.loaded = true;
	}).
	error(function(data) {
		console.log(data);
	});
});

countObjects = function (obj) {		
	var count = 0;
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }
    return count;
}