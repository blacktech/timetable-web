Login = angular.module('Login', []);

Login.controller('loginController',function($http,$scope,$location,$sessionStorage,API_HOST,appCookie,Auth) {
  	$scope.loaded = false;
  	$scope.errors = false;

  	Auth.check();
  	$scope.loaded = true;
  	

	$scope.authorize = function() {
	  	$scope.errors = false;
		$scope.error_type = undefined;
		var data = {
			login: $scope.auth.login,
			password: $scope.auth.password.md5()
		}
  		console.log(data,'auth object');
  		$http({method: 'POST',url: API_HOST+'/auth/login',data: data,headers: {'Content-Type': 'application/json'},
			}).
			success(function(data) {
				console.log(data,'login');
				if (data.response && data.reason=="all success") {
					$sessionStorage.in = true;
					$sessionStorage.data = data.user_info;
					appCookie.createCookie('id',data.uid,365);
					appCookie.createCookie('hash',data.hash,365);
					$location.path('/board');
				} else {
					
				}
			}).
			error(function(data) {
				console.log(data);
				switch(data.reason) {
					case "not_exist":
						$scope.error_type = "Такого користувача не існує";
					break;
					
					default :
						$scope.error_type = "Виникла невідома помилка";
					break;
				}
				$scope.errors = true;
			});
  	};
});