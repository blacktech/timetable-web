var evaluationChart;
Manage = angular.module('Manage', []);

Manage.controller('manageController',function($scope, $http, $sce, $modal, $sessionStorage, $timeout, API_HOST, dateFilter, Auth, $location, appCookie) {
    Auth.check();
    $scope.days = [{day:'Monday'},{day:'Tuesday'},{day:'Wednesday'},{day:'Thursday'},{day:'Friday'},{day:'Sunday'},{day:'Saturday'}];
    $scope.filter = {
        subgroup:1,
        parity:1
    };
    $scope.lessonType = [
        {value: 'lection', label: 'Лекція'},
        {value: 'practice', label: 'Практика'},
        {value: 'laboratory', label: 'Лабораторна'},
        {value: 'seminar', label: 'Семінар'}
    ];
    $scope.parityTypes = [
        {value: '1', label: 'Перша'},
        {value: '2', label: 'Друга'},
        {value: '0', label: 'Будь-яка'}
    ];
    $scope.lessonSubgroup = [
        {value: '1', label: 'Перша'},
        {value: '2', label: 'Друга'},
        {value: '0', label: 'Усі'}
    ];

    $scope.barConfig = {
        // group: 'foobar',
        animation: 150,
        handle: ".fa-bars",
        chosenClass: "text-info",
        dataIdAttr: 'data-id',
        onAdd: function (evt) {
            console.debug("onAdd",evt,"Info:"+evt.model.lesson+" "+evt.model.day);
        },
        onUpdate: function (evt) {
            console.debug("onUpdate",evt,"Info:"+evt.model.lesson+" "+evt.model.day);
            resortLessons(evt.model.day);
        },
        onRemove: function (evt) {
            console.debug("onRemove",evt,"Info:"+evt.model.lesson+" "+evt.model.day);
        },
        onSort: function (evt) {
            console.debug("onSort",evt);
        }
    };

    function resortLessons(day) {
        console.log($scope.lessons[day]);
        angular.forEach($scope.lessons[day], function(value, key) {
            value.position = key;
        });

        angular.forEach($scope.days, function(value, key) {
            console.log(key,value);
            if(value.day == day) {
                $scope.days[key].changed = true;
            }
        });

    }



    console.log($scope.lessons)
    // $scope.loaded = false;
    function readLessons() {
        $http({method: 'POST',url: API_HOST+'/manage/lessons', data: $sessionStorage.data, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            $scope.lessons = {};
            for (var i = $scope.days.length - 1; i >= 0; i--) {
                $scope.lessons[$scope.days[i].day] = [];
            };
            for (var i = data.length - 1; i >= 0; i--) {
                var lesson = data[i];
                $scope.lessons[lesson.day].push(lesson);
            };
            $scope.lessonsMirror = $scope.lessons;
        }).
        error(function(data) {
            console.log(data);
        });
    };
    readLessons();

    if ($sessionStorage.data!=undefined) {
        $scope.access = $sessionStorage.data.access;
    };




    $scope.checkEmptyArray = function(array,day) {
        // if (array.length > 0) {
        //     return false;
        // } else {
        //     return true;
        // }
    }

    $scope.editLesson=function(lesson) {
        $scope.modal_lesson = angular.copy(lesson);
        $scope.modal_lesson.room = Number($scope.modal_lesson.room);

        var time_from = $scope.modal_lesson.time_from.split(":");
        var time_to = $scope.modal_lesson.time_to.split(":");
        $scope.modal_lesson.time_from =  new Date(1970, 0, 1, time_from[0], time_from[1])
        $scope.modal_lesson.time_to =  new Date(1970, 0, 1, time_to[0], time_to[1])
    };


    $scope.saveLessonEditing = function(modal_lesson) {
        var lessonUpdatedData = {
            lesson:angular.copy(modal_lesson),
            userInfo:$sessionStorage.data
        };

        var time_to = {date:new Date(lessonUpdatedData.lesson.time_to)};
        var time_from = {date:new Date(lessonUpdatedData.lesson.time_from)};

        lessonUpdatedData.lesson.time_to = time_to.date.getHours()+":"+time_to.date.getMinutes();
        lessonUpdatedData.lesson.time_from = time_from.date.getHours()+":"+time_from.date.getMinutes();

        console.log(lessonUpdatedData);
        $http({method: 'PUT',url: API_HOST+'/manage/lesson', data: lessonUpdatedData, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            console.log(data);
            readLessons();
        }).
        error(function(data) {
            console.log(data);
        });
    };


    $scope.addLessonModal=function(day) {
        $scope.modal_lesson = {
            day:day
        };
    };

    $scope.addLesson=function(modal_lesson) {
        console.log(modal_lesson,"before");
        var lessonAddData = {
            lesson:angular.copy(modal_lesson),
            userInfo:$sessionStorage.data
        };

        var time_to = {date:new Date(lessonAddData.lesson.time_to)};
        var time_from = {date:new Date(lessonAddData.lesson.time_from)};


        if(time_to.date.getHours().length==1) {
            time_to.hours = "0"+time_to.date.getHours();
        }
        if(time_to.date.getMinutes().length==1) {
            time_to.minutes = "0"+time_to.date.getMinutes();
        }

        time_to = time_to.hours+":"+time_to.minutes;


        if(time_from.date.getHours().length==1) {
            time_from.hours = "0"+time_from.date.getHours();
        }
        if(time_from.date.getMinutes().length==1) {
            time_from.minutes = "0"+time_from.date.getMinutes();
        }

        time_from = time_from.hours+":"+time_from.minutes;

        lessonAddData.lesson.time_to = time_to;
        lessonAddData.lesson.time_from = time_from;

        console.log(lessonAddData);
        $http({method: 'POST',url: API_HOST+'/manage/lesson', data: lessonAddData, headers: {'Content-Type': 'application/json'},
        }).success(function(data) {
            console.log(data);
            readLessons();
        }).
        error(function(data) {
            console.log(data);
        });
    };

    $scope.removeLesson=function(lesson) {
        if(confirm("Delete lesson ?")) {
            $http({method: 'DELETE',url: API_HOST+'/manage/lesson/'+lesson.id, data: {userInfo:$sessionStorage.data}, headers: {'Content-Type': 'application/json'},
            }).success(function(data) {
                console.log(data);
                readLessons();
            }).
            error(function(data) {
                console.log(data);
            });
        }
    };

    function lessonSearch(searchField,searchVal) {
            console.log(searchField,searchVal);
            for (var i=0 ; i < $scope.lessons.length ; i++)
            {
                var day = $scope.lessons[i];
                console.log($scope.lessons[i]);
                for (var a = day.length - 1; a >= 0; a--) {
                    lesson = day[a];
                    console.log(lesson);
                    if(lesson[a][searchField] == searchVal) {
                        var fObj = a;
                    }
                };
            }
            return fObj;
    };
});

countObjects = function (obj) {
    var count = 0;
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }
    return count;
}
