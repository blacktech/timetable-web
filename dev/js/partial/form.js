$(".input").click(function(){
	$(this).addClass("input-filled");
	$(this).children("input").focus();
});

$(".input input").blur(function(){
	if($(this).val().length<1) {
		$(this).parent().removeClass("input-filled"); 
	} else {
		$(this).parent().addClass("input-filled");
	}
});