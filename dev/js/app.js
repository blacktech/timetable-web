App = angular.module('TimeTable', ['Configuration',
                                    'ngSanitize',
									'mgcrea.ngStrap',
                                    'ngAnimate',
                                    'ng-sortable',
									'ngRoute',
									'ngStorage',
									'Login',
									'Registration',
									'Board',
									'News',
									'Events',
									'Track',
									'Error',
                                    'Manage'
								  ]);

App.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl : 'pages/main.html',
				controller: 'MainController'
			}).when('/login', {
				templateUrl : 'pages/login.html',
				controller  : 'loginController'
			}).when('/reg', {
				templateUrl : 'pages/registration.html',
				controller  : 'registrationController'
			}).when('/board', {
				templateUrl : 'pages/board.html',
				controller  : 'boardController'
			}).when('/news', {
				templateUrl : 'pages/news.html',
				controller  : 'newsController'
			}).when('/events', {
				templateUrl : 'pages/events.html',
				controller  : 'eventsController'
			}).when('/track', {
                templateUrl : 'pages/track.html',
                controller  : 'trackController'
            }).when('/manage', {
				templateUrl : 'pages/manage.html',
				controller  : 'manageController'
			}).when('/error/:type', {
				templateUrl : 'pages/error.html',
				controller  : 'errorController'
			}).otherwise({redirectTo:'/'});
	});

App.controller('MainController',function($scope,$http,$sce,$modal,$templateCache,$location,$sessionStorage, API_HOST,Auth) {
	$scope.loaded = false;
	if ($sessionStorage.data!=undefined) {
	   	$scope.access = $sessionStorage.data.access;
	};

	$http({method: 'GET',url: API_HOST+'/news', data: '', headers: {'Content-Type': 'application/json'},
	}).success(function(data) {
  		$scope.articles = data;
		angular.forEach($scope.articles, function(news , key) {
			news.content = $sce.trustAsHtml(news.content);
		})
  		$scope.loaded = true;
	}).
	error(function(data) {
		console.log(data);
	});

	$scope.logout = function() {
		$sessionStorage.in = false;
		$sessionStorage.data = false;
		Auth.logout();
		$location.path('/');
	};

	$scope.path = location.hash.split('/')[1];


	$scope.$on('$routeChangeSuccess', function () {
            $scope.in = $sessionStorage.in;
            if ($sessionStorage.data!=undefined) {
		        $scope.access = $sessionStorage.data.access;
		    };
			$scope.path = location.hash.split('/')[1];
    });

    $scope.addEvent = function() {
    	var data = $sessionStorage.data;
    	data.time = $scope.event.time;
    	data.date = $scope.event.date;
    	data.content = $scope.event.content;
    	$http({method: 'POST',url: API_HOST+'/events/add', data: data, headers: {'Content-Type': 'application/json'},
		}).success(function(data) {
			console.log(data,'addEvent:success');
		}).
		error(function(data) {
			console.log(data,'addEvent:error');
		});
    };

});

App.controller('AppController',function($scope,$http,$sce,$modal,$templateCache,$location,$sessionStorage, API_HOST,Auth) {


});

String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
};

App.service('appCookie', function () {
        return {
        	createCookie : function(name, value, days) {
								var expires;
							    if (days) {
							        var date = new Date();
							        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
							        expires = "; expires=" + date.toGMTString();
							    }
							    else {
							        expires = "";
							    }
							    document.cookie = name + "=" + value + expires + "; path=/";
							},
			getCookie: function(c_name) {
					if (document.cookie.length > 0) {
						c_start = document.cookie.indexOf(c_name + "=");
						if (c_start != -1) {
							c_start = c_start + c_name.length + 1;
							c_end = document.cookie.indexOf(";", c_start);
							if (c_end == -1) {
								c_end = document.cookie.length;
							}
							return unescape(document.cookie.substring(c_start, c_end));
						}
					}
		    		return "";
				},
			deleteCookie: function(c_name) {
				if (document.cookie.length > 0) {
					document.cookie = c_name + "=" + "false" + "-1" + "; path=/";
				}
            },
        }
    });

App.service('Auth', function ($http,appCookie,API_HOST,$location,$sessionStorage) {
	return {
		check : function() {
			var auth = {
				"id":appCookie.getCookie('id'),
				"hash":appCookie.getCookie('hash')
			};
			console.log(auth);
			var location_hash = location.hash.split("/")[1];

			$http({method: 'POST',url: API_HOST+'/auth/check',data: auth,headers: {'Content-Type': 'application/json'},
				}).
				success(function(data) {
					console.log(data);
						$sessionStorage.in = true;
						$sessionStorage.data = data.user_info;
						if (location_hash=='login' || location_hash=='reg') {
							$location.path('/board');
						};

				}).
				error(function(data) {
					if(location_hash!='login' && location_hash!='reg'){
							$location.path('/');
						}
				});
		},
		logout: function() {
			appCookie.deleteCookie('id');
			appCookie.deleteCookie('hash');
		}
	}
});

App.service('appAssistant', function ($http,$location) {
	return {
		user : {
			notifyException:function(type) {
				$location.path('/error/'+type);
			},
		},
		logout: function() {
			appCookie.deleteCookie('id');
			appCookie.deleteCookie('hash');
		}
	}
});


App.filter('day_translate', function() {
  return function(input) {
        switch(input)
        {
            case "Monday" :
                return "Понеділок";
            break;
            case "Tuesday" :
                return "Вівторок";
            break;
            case "Wednesday" :
                return "Середа";
            break;
            case "Thursday" :
                return "Четвер";
            break;
            case "Friday" :
                return "П'ятниця";
            break;
            case "Sunday" :
                return "Субота";
            break;
            case "Saturday" :
                return "Неділя";
            break;
        }
  };
});

App.filter('lessonTypeTranslate', function() {
  return function(input) {
    	switch(input)
    	{
    		case "lection" :
    			return "Лекція";
    		break;
    		case "practice" :
    			return "Практика";
    		break;
    		case "laboratory" :
    			return "Лабораторна";
    		break;
    		case "seminar" :
    			return "Семінар";
    		break;
    	}
  };
});

App.filter('time_parse', function() {
  return function(input) {
  		split = input.split(':');
  		first = split[0];
  		second = split[1];
  		third = split[2];
    	return first+":"+second;
  };
});

App.filter('speciality', function() {
  return function(input) {
    	switch(input)
    	{
    		case "kn" :
    			return "КН";
    		break;
    	}
  };
});
App.filter('department', function() {
  return function(input) {
    	switch(input)
    	{
    		case "initki" :
    			return "ІнІТКІ";
    		break;
    	}
  };
});


String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

String.prototype.allReplace = function(obj) {
    var retStr = this
    for (var x in obj) {
      retStr = retStr.replace(new RegExp(x, 'g'), obj[x])
    }
    return retStr
}


Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}