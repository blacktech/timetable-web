module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt);
  var fs = require('fs'),
  path = require('path'),
  certsPath = path.join(__dirname, 'certs', 'server');
	grunt.initConfig({
		express: {
			options: {
				//
			},
			web: {
				options: {
					script: 'serve.js',
          args: [grunt.option('env')]
				}
			},
		},
		watch: {
			public: {
				options: {
            livereload: {
                port: 35724,
                key: fs.readFileSync(path.join(certsPath, 'my-server.key.pem')),
                cert: fs.readFileSync(path.join(certsPath, 'my-server.crt.pem')),
            }
				},
				files: [
					// triggering livereload when the .css file is updated
					// (compared to triggering when sass completes)
					// allows livereload to not do a full page refresh
					'public/css/*.css',
					'public/app/**/*.js',
					'public/**/*.html'
				]
			},
			less: {
				files: [
					'dev/styles/**/*.less'
				],
				tasks: [
					'less'
				]
			},
			jade: {
				files: [
					'dev/markup/**/*.jade'
				],
				tasks: [
					'jade'
				]
			},
      js: {
        files: [
          'dev/js/**/*.js'
        ],
        tasks: [
          'copy:js'
          // 'ngmin','uglify'
        ]
      },
			assets: {
				files: [
					'dev/assets/**/**'
				],
				tasks: [
          'copy:assets'
					// 'ngmin','uglify'
				]
			},
			web: {
				files: [
					'app/**/*.js',
					'serve.js'
				],
				tasks: [
					'express:web'
				],
				options: {
					nospawn: true, //Without this option specified express won't be reloaded
					atBegin: true,
				}
			}
		},
		concurrent: {
		    options: {
		        logConcurrentOutput: true,
		    },
		    watch: [
		        'watch:public',
		        'watch:less',
		        'watch:js',
            'watch:jade',
		        'watch:assets',
		        'watch:web'
		    ],
		},

    less: {
        build: {
          files: {
            'public/css/style.css': 'dev/styles/style.less'
          }
        }
    },

    copy: {
      js: {
        files: [{
            expand: true,
            cwd: 'dev/js/',
            src: ['**/*.js'],
            dest: 'public/app'
        }],
      },
      assets: {
        files: [{
            expand: true,
            cwd: 'dev/assets/',
            src: ['**/**'],
            dest: 'public/'
        }],
      },
    },

    jade: {
      compile: {
        options: {
          data: {
            debug: false
          }
        },
        files: {
          "public/index.html": "dev/markup/index.jade",
          "public/pages/main.html": "dev/markup/main.jade",
          "public/pages/news.html": "dev/markup/news.jade",
          "public/pages/registration.html": "dev/markup/registration.jade",
          "public/pages/login.html": "dev/markup/login.jade",
          "public/pages/board.html": "dev/markup/board.jade",
          "public/pages/events.html": "dev/markup/events.jade",
          "public/pages/track.html": "dev/markup/track.jade",
          "public/pages/error.html": "dev/markup/error.jade",
          "public/pages/manage.html": "dev/markup/manage.jade"
        }
        // files: [{
        //     expand: true,
        //     cwd: 'dev/markup',
        //     src: [ '**/*.jade' ],
        //     dest: 'public/pages',
        //     ext: '.html'
        // }]
      }
    },

    uglify: {
      prod: {
        files: [{
            expand: true,
            cwd: 'public/app',
            src: '**/*.js',
            dest: 'public/app'
        }]
      },
      vendor: {
        files: [{
            expand: true,
            cwd: 'public/vendor/js',
            src: '**/*.js',
            dest: 'public/vendor/js'
        }]
      }
    },

    cssmin: {
        options: {
        },
        build: {
            files: {
                'public/css/style.css': 'public/css/style.css'
            }
        }
    },

    clean: {
        build: {
            src: [
              "public/pages/**",
              "public/css/**"
            ]
        }
    },

    htmlmin: { // Task
        dist: { // Target
            options: { // Target options
                removeComments: true,
                collapseWhitespace: true
            },
            files: {
                "public/index.html":              "public/index.html",
                "public/pages/main.html":         "public/pages/main.html",
                "public/pages/news.html":         "public/pages/news.html",
                "public/pages/registration.html": "public/pages/registration.html",
                "public/pages/login.html":        "public/pages/login.html",
                "public/pages/board.html":        "public/pages/board.html",
                "public/pages/events.html":       "public/pages/events.html",
                "public/pages/track.html":        "public/pages/track.html"
            }
        }
    },

    ngmin: {
      controllers: {
        expand: true,
        cwd: 'dev/js/',
        src: ['**/*.js'],
        dest: 'public/app'
      }
    },

    sftp: {
      test: {
        files: {
          "./": "*json"
        },
        options: {
          path: '/tmp',
          host: '46.101.166.33',
          username: '*****',
          password: '*****',
          showProgress: true
        }
      }
    },
    sshexec: {
      test: {
        command: 'uptime',
        options: {
          host: '46.101.166.33',
          username: '*****',
          password: '*****'
        }
      }
    },

    imagemin: {                          // Task
      dynamic: {                         // Another target
        options: {                       // Target options
          optimizationLevel: 3
        },
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'dev/assets',                   // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
          dest: 'public/'                  // Destination path prefix
        }]
      }
    }

    /*deploy: {
      liveservers: {
        options:{
          servers: [{
            host: '46.101.166.33',
            port: 22,
            username: 'timetable-deploy',
            password: 'nfqvntq,k-ltgkjq'
          }],
          cmds_before_deploy: ["touch from-deploy"],
          cmds_after_deploy: ["touch after-deploy"],
          deploy_path: '/var/www/timetable/timetable-web'
        }
      }
    },*/
	});

	grunt.registerTask('web', 'launch webserver and watch tasks', [
	'parallel:web',
	]);

  grunt.registerTask('default', ['concurrent:watch','express:web']);
  grunt.registerTask('init', ['less','jade','copy:js','copy:assets']);
	grunt.registerTask('prepare', ['init','cssmin','htmlmin']);
};
