var path = require('path');
var appDir = path.dirname(require.main.filename);
// set up ========================
var express = require('express');
var helmet = require('helmet');
var app = express(); // create our app w/ express
var morgan = require('morgan'); // log requests to the console (express4)
var bodyParser = require('body-parser'); // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var database = require('./config/database');

app.use(helmet.frameguard('deny'));
app.use(helmet.xssFilter());
app.use(helmet.crossdomain());
app.use(helmet.noSniff());
app.use(helmet.hidePoweredBy({
    setTo: 'PHP 4.2.0'
}));
app.use(helmet.csp({
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'", "'unsafe-eval'", "http://localhost:35729"],
    styleSrc: ["'self'", "'unsafe-inline'", "https://fonts.googleapis.com/", "https://fonts.gstatic.com/", "http://fonts.googleapis.com/", "http://fonts.gstatic.com/"],
    imgSrc: ["'self'"],
    connectSrc: ["'self'", "ws://localhost:35729/"],
    fontSrc: ["'self'", "https://fonts.googleapis.com/", "https://fonts.gstatic.com/", "http://fonts.googleapis.com/", "http://fonts.gstatic.com/"],
    objectSrc: [],
    mediaSrc: [],
    frameSrc: []
}));

app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public/vendor')); // set the static files location /public/img will be /img for users

app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
    'extended': 'true'
})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
})); // parse application/vnd.api+json as json
app.use(methodOverride());

require('./app/routes/routes')(app);
// listen (start app with node server.js) ======================================
app.listen(1337);
console.log("App listening on port 8080");
